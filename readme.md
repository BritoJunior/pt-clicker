# Cat cat-clicker

## Description
petit jeu qui compte le nombre de clique sur une image de chat.

## Installation

1. Copier le `.env.exemple` en `.env`
1. Changer les informations d'environement dans `.env`
1. Installer les dependencies NPM `npm install`

## Dependance
*detenv - https://gitlab.com/BritoJunior/pt-clicker.git
